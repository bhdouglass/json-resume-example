# json-resume-example

An example [JSON Resume](https://jsonresume.org/) setup.
Read [my blog post](https://bhdouglass.com/blog/tech/2022-11-03-how-to-build-a-developer-json-resume/) for more details.
Also checkout [my full JSON Resume](https://gitlab.com/bhdouglass/bhdouglass-com/-/blob/master/src/data/resume.json).

## Commands

Export the `resume.json` to HTML:

```bash
npm run export:html
```

Export the `resume.json` to a PDF:

```bash
npm run export:pdf
```

## License

Copyright (C) 2024 [Brian Douglass](https://bhdouglass.com/)

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
